--liquibase formatted sql

--changeset Ilya:20201019-1
--comment add session to users
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:0 select count(*) from information_schema.columns as c where c.table_name = 'users' and c.column_name = 'session'
alter table users
    add session text;

--changeset Ilya:20201019-2
--comment add uindex to users.username
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:0 select count(*) from pg_indexes as i where i.tablename = 'usr' and i.indexname = 'users_username_uindex';
create unique index users_username_uindex
    on users (username);
