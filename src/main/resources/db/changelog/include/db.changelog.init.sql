--liquibase formatted sql

--changeset Ilya:20201001-3
--comment insert test data
insert into users (username, password)
values ('username1', 'password1');

insert into users (username, password)
values ('username2', 'password2');

insert into users (username, password)
values ('username3', 'password3');

insert into users (username, password)
values ('username4', 'password4');

insert into users (username, password)
values ('username5', 'password5');

insert into person_info (user_id, first_name, second_name, middle_name, date_birth)
values (2, 'first name 1', 'second name 1', 'middle name 1', '2020-10-01 00:00:00.000000');

insert into person_info (user_id, first_name, second_name, middle_name, date_birth)
values (1, 'first name 2', 'second name 2', 'middle name 2', '2020-10-01 00:00:00.000000');

insert into person_info (user_id, first_name, second_name, middle_name, date_birth)
values (2, 'first name 3', 'second name 3', 'middle name 3', '2020-10-01 00:00:00.000000');

insert into person_info (user_id, first_name, second_name, middle_name, date_birth)
values (3, 'first name 4', 'second name 4', 'middle name 4', '2020-10-02 00:00:00.000000');

insert into person_info (user_id, first_name, second_name, middle_name, date_birth)
values (4, 'first name 5', 'second name 5', 'middle name 5', '2020-10-01 00:00:00.000000');

insert into bank (bank_id, name)
values (1, 'bank name 1');

insert into bank (bank_id, name)
values (2, 'bank name 2');

insert into bank (bank_id, name)
values (3, 'bank name 3');

insert into bank (bank_id, name)
values (4, 'bank name 4');

insert into bank (bank_id, name)
values (5, 'bank name 5');

insert into person_info_bank (person_info_id, bank_id, connection_date)
values (1, 2, '2020-10-01 00:00:00.000000');

insert into person_info_bank (person_info_id, bank_id, connection_date)
values (1, 4, '2020-10-01 00:00:00.000000');

insert into person_info_bank (person_info_id, bank_id, connection_date)
values (2, 1, '2020-10-01 00:00:00.000000');

insert into person_info_bank (person_info_id, bank_id, connection_date)
values (3, 4, '2020-10-01 00:00:00.000000');

insert into person_info_bank (person_info_id, bank_id, connection_date)
values (4, 5, '2020-10-01 00:00:00.000000');
