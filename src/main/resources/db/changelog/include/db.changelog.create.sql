--liquibase formatted sql

--changeset Ilya:20201001-1
--comment create users table
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:0 select count(*) from pg_tables where tablename = 'users';
create table users
(
    user_id  serial
        primary key,
    username text not null,
    password text not null
);

--changeset Ilya:20201001-2
--comment create person_info table
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:0 select count(*) from pg_tables where tablename = 'person_info';
create table person_info
(
    person_info_id serial
        primary key,
    user_id        int
        references users (user_id),
    first_name     text      not null,
    second_name    text      not null,
    middle_name    text      not null,
    date_birth     timestamp not null
);

--changeset Ilya:20201020-1
--comment create bank table
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:0 select count(*) from pg_tables where tablename = 'bank';
create table bank
(
    bank_id serial
        primary key,
    name    text not null
);

--changeset Ilya:20201020-2
--comment create person_info_bank table
--preconditions onFail:MARK_RAN
--precondition-sql-check expectedResult:0 select count(*) from pg_tables where tablename = 'person_info_bank';
create table person_info_bank
(
    person_info_bank_id serial
        primary key,
    person_info_id      int       not null
        references person_info (person_info_id),
    bank_id             int       not null
        references bank (bank_id),
    connection_date     timestamp not null,
    unique (person_info_id, bank_id)
);