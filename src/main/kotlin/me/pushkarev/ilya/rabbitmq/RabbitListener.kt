package me.pushkarev.ilya.rabbitmq

import me.pushkarev.ilya.config.RabbitConfiguration
import me.pushkarev.ilya.model.dto.ChangeBankDto
import me.pushkarev.ilya.service.api.PersonInfoService
import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component

@Component
@EnableRabbit
class RabbitListener(private val personInfoService: PersonInfoService) {

    @RabbitListener(queues = [RabbitConfiguration.CHANGE_BANK_QUEUE_NAME])
    fun changeBankQueue(changeBankDto: ChangeBankDto) {
        personInfoService.bankChangeProcess(changeBankDto)
    }
}
