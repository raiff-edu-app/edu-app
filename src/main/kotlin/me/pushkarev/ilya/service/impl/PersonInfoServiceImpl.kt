package me.pushkarev.ilya.service.impl

import me.pushkarev.ilya.config.RabbitConfiguration
import me.pushkarev.ilya.model.domain.Bank
import me.pushkarev.ilya.model.domain.PersonInfo
import me.pushkarev.ilya.model.domain.PersonInfoBank
import me.pushkarev.ilya.model.domain.User
import me.pushkarev.ilya.model.dto.BankDto
import me.pushkarev.ilya.model.dto.ChangeBankDto
import me.pushkarev.ilya.model.dto.PersonInfoDto
import me.pushkarev.ilya.repository.PersonInfoBankRepository
import me.pushkarev.ilya.repository.PersonInfoRepository
import me.pushkarev.ilya.service.api.BankService
import me.pushkarev.ilya.service.api.PersonInfoService
import org.springframework.amqp.AmqpException
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class PersonInfoServiceImpl(
    private val bankService: BankService,
    private val amqpTemplate: AmqpTemplate,
    private val personInfoRepository: PersonInfoRepository,
    private val personInfoBankRepository: PersonInfoBankRepository,
) : PersonInfoService {

    override fun getAll(authUser: User): Set<PersonInfo>? {
        return personInfoRepository.findAllByUser(authUser)
    }

    override fun getById(id: Long, authUser: User): PersonInfo? {
        return personInfoRepository.findByPersonInfoIdAndUser(id, authUser)
    }

    override fun filter(filter: String, authUser: User): Set<PersonInfo>? {
        return personInfoRepository.findByAllFields(filter, authUser.userId!!)
    }

    override fun bankChange(changeBankDto: ChangeBankDto): Boolean {
        return try {
            amqpTemplate.convertAndSend(RabbitConfiguration.CHANGE_BANK_QUEUE_NAME, changeBankDto)
            true
        } catch (e: AmqpException) {
            println(e)
            false
        }
    }

    @Transactional
    override fun bankChangeProcess(changeBankDto: ChangeBankDto) {
        val personInfo: PersonInfo? = personInfoRepository.findById(changeBankDto.personInfoId).get()
        if (personInfo != null) {
            val oldBank: Bank = bankService.getById(changeBankDto.oldBankId)
            val newBank: Bank = bankService.getById(changeBankDto.newBankId)

            val personInfoBank = personInfo.personInfoBank?.firstOrNull { it.bank == oldBank }
            if (personInfoBank != null) {
                personInfo.personInfoBank?.remove(personInfoBank)
                personInfoBankRepository.deleteById(personInfoBank.personInfoBankId!!)

                personInfo.personInfoBank?.add(PersonInfoBank(null, personInfo, newBank, Date()))
                personInfoRepository.save(personInfo)
            }
        }
    }

    @Transactional
    override fun create(personInfoDto: PersonInfoDto, authUser: User): PersonInfo {
        val personInfoBanks: MutableSet<PersonInfoBank> = mutableSetOf()

        val personInfo = PersonInfo(
            null,
            authUser,
            personInfoDto.firstName,
            personInfoDto.secondName,
            personInfoDto.middleName,
            personInfoDto.dateBirth,
            personInfoBanks
        )

        if (personInfoDto.banks != null) {
            for (bank: BankDto in personInfoDto.banks!!) {
                val newPersonInfoBank = if (bank.bankId != null) {
                    PersonInfoBank(null, personInfo, bankService.getById(bank.bankId!!), Date())
                } else {
                    val newBank: Bank = bankService.create(Bank(null, null, bank.name))
                    val newPersonInfoBank = PersonInfoBank(null, personInfo, newBank, Date())
                    newPersonInfoBank.bank.personInfoBank = mutableSetOf(newPersonInfoBank)
                    newPersonInfoBank
                }

                personInfoBanks.add(newPersonInfoBank)
            }
        }

        return personInfoRepository.save(personInfo)
    }

    override fun update(personInfo: PersonInfo, authUser: User): PersonInfo {
        return personInfoRepository.save(personInfo)
    }

    override fun delete(id: Long, authUser: User) {
        personInfoRepository.deleteById(id)
    }
}
