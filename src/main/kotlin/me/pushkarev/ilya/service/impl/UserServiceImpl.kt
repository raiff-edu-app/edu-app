package me.pushkarev.ilya.service.impl

import me.pushkarev.ilya.exception.UserNotFoundException
import me.pushkarev.ilya.model.domain.User
import me.pushkarev.ilya.repository.UserRepository
import me.pushkarev.ilya.service.api.UserService
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserServiceImpl(private val userRepository: UserRepository) : UserService {

    override fun getBySession(session: String): User? {
        return userRepository.findBySession(session)
    }

    override fun registration(user: User): User {
        return userRepository.save(user)
    }

    override fun login(user: User): User {
        val findUser: User = userRepository.findByUsernameAndPassword(user.username, user.password)
            ?: throw UserNotFoundException()

        findUser.session = UUID.randomUUID().toString()
        userRepository.save(findUser)

        return findUser
    }

    override fun logout(user: User): Boolean {
        user.session = null
        userRepository.save(user)
        return true
    }

    override fun update(user: User, authUser: User): User {
        return userRepository.save(user)
    }

    override fun delete(user: User) {
        userRepository.delete(user)
    }
}
