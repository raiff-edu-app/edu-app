package me.pushkarev.ilya.service.impl

import me.pushkarev.ilya.model.domain.Bank
import me.pushkarev.ilya.repository.BankRepository
import me.pushkarev.ilya.service.api.BankService
import org.springframework.stereotype.Service

@Service
class BankServiceImpl(private val bankRepository: BankRepository) : BankService {

    override fun getAll(): Set<Bank> {
        return bankRepository.findAll().toSet()
    }

    override fun getById(id: Long): Bank {
        return bankRepository.findById(id).get()
    }

    override fun create(bank: Bank): Bank {
        return bankRepository.save(bank)
    }

    override fun update(bank: Bank): Bank {
        return bankRepository.save(bank)
    }

    override fun delete(id: Long) {
        bankRepository.deleteById(id)
    }
}
