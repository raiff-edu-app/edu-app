package me.pushkarev.ilya.service.api

import me.pushkarev.ilya.model.domain.Bank

interface BankService {

    fun getAll(): Set<Bank>

    fun getById(id: Long): Bank

    fun create(bank: Bank): Bank

    fun update(bank: Bank): Bank

    fun delete(id: Long)
}
