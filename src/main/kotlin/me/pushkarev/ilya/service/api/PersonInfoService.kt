package me.pushkarev.ilya.service.api

import me.pushkarev.ilya.model.domain.PersonInfo
import me.pushkarev.ilya.model.domain.User
import me.pushkarev.ilya.model.dto.ChangeBankDto
import me.pushkarev.ilya.model.dto.PersonInfoDto

interface PersonInfoService {

    fun getAll(authUser: User): Set<PersonInfo>?

    fun getById(id: Long, authUser: User): PersonInfo?

    fun filter(filter: String, authUser: User): Set<PersonInfo>?

    fun bankChange(changeBankDto: ChangeBankDto): Boolean

    fun bankChangeProcess(changeBankDto: ChangeBankDto)

    fun create(personInfoDto: PersonInfoDto, authUser: User): PersonInfo

    fun update(personInfo: PersonInfo, authUser: User): PersonInfo

    fun delete(id: Long, authUser: User)
}
