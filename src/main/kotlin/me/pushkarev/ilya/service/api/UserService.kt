package me.pushkarev.ilya.service.api

import me.pushkarev.ilya.model.domain.User

interface UserService {

    fun getBySession(session: String): User?

    fun registration(user: User): User

    fun login(user: User): User

    fun logout(user: User): Boolean

    fun update(user: User, authUser: User): User

    fun delete(user: User)
}
