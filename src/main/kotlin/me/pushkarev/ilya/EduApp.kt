package me.pushkarev.ilya

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EduApp

fun main(args: Array<String>) {
    runApplication<EduApp>(*args)
}
