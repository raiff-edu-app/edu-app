package me.pushkarev.ilya.model.domain

import com.fasterxml.jackson.annotation.JsonManagedReference
import javax.persistence.*

@Entity
@Table(name = "users")
class User(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var userId: Long?,

    @Column(nullable = false)
    var username: String,

    @Column(nullable = false)
    var password: String,

    var session: String?,

    @JsonManagedReference
    @OneToMany(mappedBy = "user", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var personsInfo: MutableSet<PersonInfo>?,
)
