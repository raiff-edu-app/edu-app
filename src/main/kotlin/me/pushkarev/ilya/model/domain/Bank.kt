package me.pushkarev.ilya.model.domain

import com.fasterxml.jackson.annotation.JsonBackReference
import javax.persistence.*

@Entity
@Table(name = "bank")
class Bank(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var bankId: Long?,

    @JsonBackReference
    @OneToMany(mappedBy = "bank", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var personInfoBank: MutableSet<PersonInfoBank>?,

    @Column(nullable = false)
    var name: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Bank

        if (bankId != other.bankId) return false

        return true
    }

    override fun hashCode(): Int {
        return bankId?.hashCode() ?: 0
    }
}
