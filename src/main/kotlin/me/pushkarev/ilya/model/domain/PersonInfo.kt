package me.pushkarev.ilya.model.domain

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonManagedReference
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "person_info")
class PersonInfo(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var personInfoId: Long?,

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "user_id")
    var user: User,

    @Column(nullable = false)
    var firstName: String,

    @Column(nullable = false)
    var secondName: String,

    @Column(nullable = false)
    var middleName: String,

    @Column(nullable = false)
    var dateBirth: Date,

    @JsonManagedReference
    @OneToMany(mappedBy = "personInfo", cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    var personInfoBank: MutableSet<PersonInfoBank>?,
)
