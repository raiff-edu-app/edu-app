package me.pushkarev.ilya.model.domain

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonManagedReference
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "person_info_bank")
class PersonInfoBank(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var personInfoBankId: Long?,

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "person_info_id")
    var personInfo: PersonInfo,

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "bank_id")
    var bank: Bank,

    @Column(nullable = false)
    var connectionDate: Date,
)
