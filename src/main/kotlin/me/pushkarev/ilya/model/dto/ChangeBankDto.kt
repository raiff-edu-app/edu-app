package me.pushkarev.ilya.model.dto

import java.io.Serializable

class ChangeBankDto(val personInfoId: Long, val oldBankId: Long, val newBankId: Long) : Serializable
