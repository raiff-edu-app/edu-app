package me.pushkarev.ilya.model.dto

import java.util.*

class PersonInfoDto(
    var personInfoId: Long?,
    var userDto: UserDto?,
    var firstName: String,
    var secondName: String,
    var middleName: String,
    var dateBirth: Date,
    var banks: MutableSet<BankDto>?
)
