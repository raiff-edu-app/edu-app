package me.pushkarev.ilya.model.dto

import org.springframework.http.HttpStatus

class ErrorRequestDto(val code: Int, val status: HttpStatus, val message: String)