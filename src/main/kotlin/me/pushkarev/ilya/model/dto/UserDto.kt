package me.pushkarev.ilya.model.dto

class UserDto(
    var userId: Long?,
    var username: String,
    var password: String,
)
