package me.pushkarev.ilya.repository

import me.pushkarev.ilya.model.domain.PersonInfo
import me.pushkarev.ilya.model.domain.User
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface PersonInfoRepository : CrudRepository<PersonInfo, Long> {

    fun findAllByUser(authUser: User): Set<PersonInfo>?

    fun findByPersonInfoIdAndUser(personInfoId: Long, user: User): PersonInfo?

    @Query(
        value = "select pi.* " +
                "from person_info pi " +
                "         left outer join person_info_bank pib on pi.person_info_id = pib.person_info_id " +
                "         left outer join bank b on pib.bank_id = b.bank_id " +
                "where pi.user_id = :userId " +
                "  and (pi.first_name ilike %:filter% " +
                "    or pi.second_name ilike %:filter% " +
                "    or pi.middle_name ilike %:filter% " +
                "    or b.name ilike %:filter%);",
        nativeQuery = true
    )
    fun findByAllFields(@Param("filter") filter: String, @Param("userId") authUserId: Long): Set<PersonInfo>?
}
