package me.pushkarev.ilya.repository

import me.pushkarev.ilya.model.domain.PersonInfoBank
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface PersonInfoBankRepository : CrudRepository<PersonInfoBank, Long>
