package me.pushkarev.ilya.repository

import me.pushkarev.ilya.model.domain.User
import org.springframework.data.repository.CrudRepository

interface UserRepository : CrudRepository<User, Long> {

    fun findBySession(session: String): User?

    fun findByUsernameAndPassword(username: String, password: String): User?
}
