package me.pushkarev.ilya.repository

import me.pushkarev.ilya.model.domain.Bank
import org.springframework.data.repository.CrudRepository

interface BankRepository : CrudRepository<Bank, Long>
