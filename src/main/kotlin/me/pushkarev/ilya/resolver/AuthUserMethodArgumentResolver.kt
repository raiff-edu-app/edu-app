package me.pushkarev.ilya.resolver

import me.pushkarev.ilya.annotation.AuthUser
import me.pushkarev.ilya.exception.NotAuthorizedException
import me.pushkarev.ilya.service.api.UserService
import me.pushkarev.ilya.utils.UserUtils
import org.springframework.core.MethodParameter
import org.springframework.web.bind.support.WebDataBinderFactory
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.method.support.ModelAndViewContainer
import javax.servlet.http.HttpServletRequest

class AuthUserMethodArgumentResolver(private val userService: UserService) : HandlerMethodArgumentResolver {

    override fun supportsParameter(parameter: MethodParameter): Boolean {
        return parameter.getParameterAnnotation(AuthUser::class.java) != null
    }

    override fun resolveArgument(
        parameter: MethodParameter,
        mavContainer: ModelAndViewContainer?,
        webRequest: NativeWebRequest,
        binderFactory: WebDataBinderFactory?
    ): Any? {
        val javaSessionId: String = UserUtils.getCookie(webRequest.nativeRequest as HttpServletRequest)
        return userService.getBySession(javaSessionId) ?: throw NotAuthorizedException()
    }
}
