package me.pushkarev.ilya.config

import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.QueueBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component

@Component
@Configuration
class RabbitConfiguration {

    companion object {
        const val CHANGE_BANK_QUEUE_NAME = "change.bank.queue"
    }

    @Bean
    fun queue(): Queue {
        return QueueBuilder.nonDurable(CHANGE_BANK_QUEUE_NAME).build()
    }
}
