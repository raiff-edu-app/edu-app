package me.pushkarev.ilya.config

import me.pushkarev.ilya.resolver.AuthUserMethodArgumentResolver
import me.pushkarev.ilya.service.api.UserService
import org.springframework.context.annotation.Configuration
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
class MvcConfiguration(private val userService: UserService) : WebMvcConfigurer {

    override fun addArgumentResolvers(resolvers: MutableList<HandlerMethodArgumentResolver>) {
        resolvers.add(AuthUserMethodArgumentResolver(userService))
    }
}
