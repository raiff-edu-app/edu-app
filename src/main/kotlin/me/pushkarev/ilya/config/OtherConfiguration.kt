package me.pushkarev.ilya.config

import org.modelmapper.ModelMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OtherConfiguration {

    @Bean
    fun modelMapper(): ModelMapper {
        return ModelMapper()
    }
}