package me.pushkarev.ilya.controller

import me.pushkarev.ilya.annotation.AuthUser
import me.pushkarev.ilya.model.domain.PersonInfo
import me.pushkarev.ilya.model.domain.User
import me.pushkarev.ilya.model.dto.ChangeBankDto
import me.pushkarev.ilya.model.dto.PersonInfoDto
import me.pushkarev.ilya.service.api.PersonInfoService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/person-info")
class PersonInfoController(private val personInfoService: PersonInfoService) {

    @GetMapping
    fun getAll(@AuthUser authUser: User): Set<PersonInfo>? {
        return personInfoService.getAll(authUser)
    }

    @GetMapping("/{id}")
    fun getById(@PathVariable id: Long, @AuthUser authUser: User): PersonInfo? {
        return personInfoService.getById(id, authUser)
    }

    @GetMapping("/filter")
    fun filter(@RequestParam filter: String, @AuthUser authUser: User): Set<PersonInfo>? {
        return personInfoService.filter(filter, authUser)
    }

    @PostMapping("/change-bank")
    fun bankChange(@RequestBody changeBankDto: ChangeBankDto, @AuthUser authUser: User): Boolean {
        return personInfoService.bankChange(changeBankDto)
    }

    @PostMapping
    fun create(@RequestBody personInfoDto: PersonInfoDto, @AuthUser authUser: User): PersonInfo {
        return personInfoService.create(personInfoDto, authUser)
    }

    @PutMapping
    fun update(@RequestBody personInfo: PersonInfo, @AuthUser authUser: User): PersonInfo {
        return personInfoService.update(personInfo, authUser)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long, @AuthUser authUser: User) {
        personInfoService.delete(id, authUser)
    }
}
