package me.pushkarev.ilya.controller

import me.pushkarev.ilya.annotation.AuthUser
import me.pushkarev.ilya.model.domain.User
import me.pushkarev.ilya.model.dto.UserDto
import me.pushkarev.ilya.service.api.UserService
import me.pushkarev.ilya.utils.UserUtils
import org.modelmapper.ModelMapper
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping("/user")
class UserController(
    private val userService: UserService,
    private val modelMapper: ModelMapper,
) {

    @GetMapping
    fun get(@AuthUser authUser: User): User {
        return authUser
    }

    @PostMapping("/registration")
    fun registration(@RequestBody userDto: UserDto, response: HttpServletResponse): User {
        val user: User = userService.registration(modelMapper.map(userDto, User::class.java))
        UserUtils.addCookie(response, user.session!!)
        return user
    }

    @PostMapping("/login")
    fun login(@RequestBody userDto: UserDto, response: HttpServletResponse): User {
        val user: User = userService.login(modelMapper.map(userDto, User::class.java))
        UserUtils.addCookie(response, user.session!!)
        return user
    }

    @GetMapping("/logout")
    fun logout(response: HttpServletResponse, @AuthUser authUser: User) {
        if (userService.logout(authUser)) {
            UserUtils.deleteCookie(response)
        }
    }

    @PutMapping
    fun update(@RequestBody userDto: UserDto, @AuthUser user: User): User {
        return userService.update(modelMapper.map(userDto, User::class.java), user)
    }

    @DeleteMapping("/{id}")
    fun delete(@AuthUser user: User) {
        userService.delete(user)
    }
}
