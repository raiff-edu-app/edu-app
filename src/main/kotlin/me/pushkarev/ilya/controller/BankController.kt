package me.pushkarev.ilya.controller

import me.pushkarev.ilya.model.domain.Bank
import me.pushkarev.ilya.service.api.BankService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/bank")
class BankController(private val bankService: BankService) {

    @GetMapping
    fun getAll(): Set<Bank> {
        return bankService.getAll()
    }

    @GetMapping("/{id}")
    fun getById(@PathVariable id: Long): Bank {
        return bankService.getById(id)
    }

    @PostMapping
    fun create(@RequestBody bank: Bank): Bank {
        return bankService.create(bank)
    }

    @PutMapping
    fun update(@RequestBody bank: Bank): Bank {
        return bankService.update(bank)
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) {
        return bankService.delete(id)
    }
}
