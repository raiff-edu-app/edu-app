package me.pushkarev.ilya.controller

import me.pushkarev.ilya.exception.NotAuthorizedException
import me.pushkarev.ilya.exception.UserNotFoundException
import me.pushkarev.ilya.model.dto.ErrorRequestDto
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class ExceptionHandlerController {

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(NotAuthorizedException::class)
    fun notAuthorizedException(notAuthorizedException: NotAuthorizedException): ErrorRequestDto {
        return ErrorRequestDto(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED, notAuthorizedException.message)
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(UserNotFoundException::class)
    fun userNotFoundException(userNotFoundException: UserNotFoundException): ErrorRequestDto {
        return ErrorRequestDto(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, userNotFoundException.message)
    }
}
