package me.pushkarev.ilya.exception

class NotAuthorizedException : Exception() {

    override val message: String
        get() = "Не авторизован"
}
