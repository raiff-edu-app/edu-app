package me.pushkarev.ilya.exception

class UserNotFoundException : Exception() {

    override val message: String
        get() = "Пользователь не найден"
}
