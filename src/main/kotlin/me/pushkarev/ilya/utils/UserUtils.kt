package me.pushkarev.ilya.utils

import me.pushkarev.ilya.exception.NotAuthorizedException
import org.springframework.stereotype.Component
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class UserUtils {

    companion object {
        private const val JAVASESSIONID: String = "JAVASESSIONID"

        fun addCookie(response: HttpServletResponse, javaSessionId: String) {
            setCookie(javaSessionId, 1 * 24 * 60 * 60, response)
        }

        fun getCookie(request: HttpServletRequest): String {
            if (request.cookies != null) {
                for (cookie: Cookie in request.cookies) {
                    if (cookie.name == JAVASESSIONID) {
                        return cookie.value
                    }
                }
            }

            throw NotAuthorizedException()
        }

        fun deleteCookie(response: HttpServletResponse) {
            setCookie(null, 0, response)
        }

        private fun setCookie(value: String?, maxAge: Int, response: HttpServletResponse) {
            val cookie = Cookie(JAVASESSIONID, value)
            cookie.path = "/"
            cookie.maxAge = maxAge
            response.addCookie(cookie)
        }
    }
}
